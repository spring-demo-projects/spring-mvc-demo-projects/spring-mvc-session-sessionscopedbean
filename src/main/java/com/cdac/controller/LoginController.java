package com.cdac.controller;

import javax.inject.Provider;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.cdac.model.LoginPageModel;
import com.cdac.model.PersonalDetails;
import com.cdac.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	@Autowired
	Provider<PersonalDetails> personalDetails;

	@GetMapping(value = "login", produces = "text/html")
	public String showLoginPage(Model model) {

		model.addAttribute("command", new LoginPageModel());
		return "loginPage";
	}

	@PostMapping(value = "login", produces = "text/html")
	public String login(@ModelAttribute LoginPageModel loginPageModel, HttpSession httpSession, Model model) {
		if (loginPageModel.getEmail().equals(loginPageModel.getPassword())) {
			personalDetails.get().setName(loginService.getName());
			personalDetails.get().setEmail(loginPageModel.getEmail());
			personalDetails.get().setAddharNumber(loginService.getAddharNumber());
			personalDetails.get().setMobileNumber(loginService.getMobileNumber());
			personalDetails.get().setPanNumber(loginService.getPanNumber());
			model.addAttribute("name", personalDetails.get().getName());
			return "welcome";
		} else
			return "loginFailed";
	}

	@GetMapping(value = "dashboard", produces = "text/html")
	public String showDashboard(Model model) {
		model.addAttribute("name", personalDetails.get().getEmail());
		model.addAttribute("email", personalDetails.get().getEmail());
		model.addAttribute("addharNumber", personalDetails.get().getEmail());
		model.addAttribute("mobileNumber", personalDetails.get().getEmail());
		model.addAttribute("panNumber", personalDetails.get().getEmail());
		return "dashboard";

	}

	@GetMapping(value = "logout", produces = "text/html")
	public String logout(HttpSession httpSession) {
		httpSession.invalidate();
		return "logoutPage";
	}

}
