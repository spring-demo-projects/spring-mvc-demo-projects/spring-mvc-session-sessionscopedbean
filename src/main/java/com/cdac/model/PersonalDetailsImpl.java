package com.cdac.model;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Scope(scopeName = "session", proxyMode = ScopedProxyMode.INTERFACES)
@Component
public class PersonalDetailsImpl implements PersonalDetails {

	private String name;
	private String email;
	private String addharNumber;
	private String mobileNumber;
	private String panNumber;

	public PersonalDetailsImpl() {

	}

	public PersonalDetailsImpl(String name, String email, String addharNumber, String mobileNumber, String panNumber) {
		super();
		this.name = name;
		this.email = email;
		this.addharNumber = addharNumber;
		this.mobileNumber = mobileNumber;
		this.panNumber = panNumber;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getAddharNumber() {
		return addharNumber;
	}

	@Override
	public void setAddharNumber(String addharNumber) {
		this.addharNumber = addharNumber;
	}

	@Override
	public String getMobileNumber() {
		return mobileNumber;
	}

	@Override
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Override
	public String getPanNumber() {
		return panNumber;
	}

	@Override
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	@Override
	public String toString() {
		return "PersonalDetails [name=" + name + ", email=" + email + ", addharNumber=" + addharNumber
				+ ", mobileNumber=" + mobileNumber + ", panNumber=" + panNumber + "]";
	}

}
