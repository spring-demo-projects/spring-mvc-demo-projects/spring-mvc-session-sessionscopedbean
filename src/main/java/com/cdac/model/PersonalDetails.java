package com.cdac.model;

public interface PersonalDetails {

	String getName();

	void setName(String name);

	String getEmail();

	void setEmail(String email);

	String getAddharNumber();

	void setAddharNumber(String addharNumber);

	String getMobileNumber();

	void setMobileNumber(String mobileNumber);

	String getPanNumber();

	void setPanNumber(String panNumber);

	String toString();

}