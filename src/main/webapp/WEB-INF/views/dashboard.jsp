<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Dashboard</title>
</head>
<body>

	<div>
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Addhar Number</th>
					<th>Mobile Number</th>
					<th>Pan Number</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>${name }</td>
					<td>${email }</td>
					<td>${addharNumber }</td>
					<td>${mobileNumber }</td>
					<td>${panNumber }</td>
				</tr>
			</tbody>
		</table>
		<br> <a href="logout">Logout</a>
	</div>

</body>
</html>