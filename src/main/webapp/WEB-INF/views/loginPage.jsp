<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
</head>
<body>

	<div>
		<springForm:form action="login">

			<div>
				<springForm:label path="email" for="email">Email : </springForm:label>
				<springForm:input path="email" />
			</div>
			<div>
				<springForm:label path="password" for="password">Password : </springForm:label>
				<springForm:input path="password" />
			</div>
			<div>
				<input type="submit" value="Login">
			</div>

		</springForm:form>
	</div>


</body>
</html>